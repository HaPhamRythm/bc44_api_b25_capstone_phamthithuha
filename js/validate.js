var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = `<strong>${message}</strong>`;
};

var checkDuplicate = function (prID, list) {
  var index = list.findIndex(function (item) {
    return prID == item.id;
  });
  if (index == -1) {
    showMessage("spanPrID", "");
    return true;
  } else {
    showMessage("spanPrID", "The ID is existing, please choose another.");
    return false;
  }
};

var checkBlank = function (id, value) {
  if (value.length == 0) {
    showMessage(id, "Please fulfil the field.");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
};

var checkPrice = function (id, value) {
  var integer = /^[0-9]/;

  if (integer.test(value)) {
    showMessage(id, "");
    return true;
  } else {
    showMessage(id, "Please enter number 0-9");
    return false;
  }
};

var checkType = function (id, prType) {
  if (prType == 0) {
    showMessage(id, "Please choose type");
    return false;
  } else {
    showMessage(id, "");
    return true;
  }
};
