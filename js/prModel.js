function Product(
  _id,
  _price,
  _name,
  _screen,
  _blCamera,
  _frCamera,
  _img,
  _desc,
  _type,
  _typeText
) {
  this.id = _id;
  this.price = _price;
  this.name = _name;
  this.screen = _screen;
  this.blCamera = _blCamera;
  this.frCamera = _frCamera;
  this.img = _img;
  this.desc = _desc;
  this.type = _type;
  this.typeText = _typeText;
}
